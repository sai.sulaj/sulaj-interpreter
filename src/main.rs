#![allow(dead_code)]

use std::{
    env,
    process,
};

extern crate text_io;

mod sulaj_interpreter;
use sulaj_interpreter::{
    interpreter::SulajInterpreter,
};

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut interpreter = SulajInterpreter::new();

    if args.len() > 2 {
        println!("Usage: sulaj [script]");
        process::exit(64);
    } else if args.len() == 2 {
        interpreter.run_file(&args[1]);
    } else {
        interpreter.run_prompt();
    }
}
