use std::{
    fs,
    process::exit,
};
use text_io::*;
use crate::sulaj_interpreter::{
    scanner::Scanner,
    token::Token,
    literal,
    expression::{
        Visitor,
        Binary,
        Grouping,
        Literal,
        Unary,
        Expr,
    },
};

pub struct SulajInterpreter {
    had_error: bool,
}

impl SulajInterpreter {
    pub fn new() -> Self {
        SulajInterpreter {
            had_error: false,
        }
    }
    pub fn run_prompt(&mut self) {
        let mut input: String = String::new();

        loop {
            println!("> ");
            input = read!("{}\n");
            self.run(&input);
        }
    }
    pub fn run(&mut self, input: &str) {
        let mut scanner = Scanner::new(input);
        let mut tokens: Vec<Token> = Vec::new();
        self.had_error = scanner.scan_tokens(&mut tokens);

        tokens.iter().for_each(|token| {
            println!("{}", token.lexeme);
        });

        if self.had_error {
            exit(65);
        }
    }
    pub fn run_file(&mut self, path: &str) {
        let input = fs::read_to_string(path).expect("Could not read file.");
        self.run(&input);
    }
    pub fn error(&mut self, line: usize, message: &str) {
        self.report(line, "", message);
    }
    fn report(&mut self, line: usize, location: &str, message: &str) {
        println!("[line {}] Error{}: {}", line, location, message);
        self.had_error = true;
    }
}
