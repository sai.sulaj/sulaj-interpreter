use std::collections::HashMap;
use crate::sulaj_interpreter::token_type::TokenType;

pub fn get_reserved() -> HashMap<String, TokenType> {
    let mut reserved: HashMap<String, TokenType> = HashMap::new();

    reserved.insert("and".to_owned(), TokenType::AND);
    reserved.insert("class".to_owned(), TokenType::CLASS);
    reserved.insert("else".to_owned(), TokenType::ELSE);
    reserved.insert("false".to_owned(), TokenType::FALSE);
    reserved.insert("for".to_owned(), TokenType::FOR);
    reserved.insert("fun".to_owned(), TokenType::FUN);
    reserved.insert("if".to_owned(), TokenType::IF);
    reserved.insert("nil".to_owned(), TokenType::NIL);
    reserved.insert("or".to_owned(), TokenType::OR);
    reserved.insert("print".to_owned(), TokenType::PRINT);
    reserved.insert("return".to_owned(), TokenType::RETURN);
    reserved.insert("super".to_owned(), TokenType::SUPER);
    reserved.insert("this".to_owned(), TokenType::THIS);
    reserved.insert("true".to_owned(), TokenType::TRUE);
    reserved.insert("var".to_owned(), TokenType::VAR);
    reserved.insert("while".to_owned(), TokenType::WHILE);

    reserved
}
