use std::{
    rc::Rc,
    cell::RefCell,
};

use crate::sulaj_interpreter::{
    token::Token,
    token_type::TokenType,
    literal,
    expression,
    expression::{
        Expr,
        Literal,
        Binary,
        Unary,
        Grouping,
    },
};

pub struct Parser {
    tokens: Vec<Token>,
    current: usize,
}

impl Parser {
    pub fn new(tokens: Vec<Token>) -> Self {
        Parser {
            tokens,
            current: 0,
        }
    }

    fn primary(&mut self) -> Expr {
        if self.match_type(&[&TokenType::FALSE]) {
            return Expr::LiteralExpr(Literal {
                value: literal::Literal::Bool(true),
            });
        }
        if self.match_type(&[&TokenType::TRUE]) {
            return Expr::LiteralExpr(Literal {
                value: literal::Literal::Bool(true),
            });
        }
        if self.match_type(&[&TokenType::NIL]) {
            return Expr::LiteralExpr(Literal {
                value: literal::Literal::None,
            });
        }

        if self.match_type(&[&TokenType::NUMBER, &TokenType::STRING]) {
            return Expr::LiteralExpr(Literal {
                value: self.previous().literal.unwrap(),
            });
        }

        if self.match_type(&[&TokenType::LEFT_PAREN]) {
            let expr: Expr = self.expression();
            self.consume(TokenType::RIGHT_PAREN, "Expect ')' after expression");
            return Expr::GroupingExpr(Grouping {
                expression: Rc::new(RefCell::new(expr)),
            });
        }

        panic!("Primary expression found no valid end");
    }

    fn consume(&self, token_type: TokenType, err: &str) -> Token {
        if self.check(token_type) { return self.advance(); }

        // TODO: Continue.
    }

    fn unary(&mut self) -> Expr {
        if self.match_type(&[&TokenType::BANG, &TokenType::MINUS]) {
            let operator: Token = self.previous();
            let right: Expr = self.unary();
            return Expr::UnaryExpr(Unary {
                operator,
                right: Rc::new(RefCell::new(right)),
            })
        }

        self.primary()
    }

    fn multiplication(&mut self) -> Expr {
        let mut expr: Expr = self.unary();

        while self.match_type(&[&TokenType::SLASH, &TokenType::STAR]) {
            let operator: Token = self.previous();
            let right: Expr = self.unary();
            expr = Expr::BinaryExpr(Binary {
                left: Rc::new(RefCell::new(expr)),
                operator,
                right: Rc::new(RefCell::new(right)),
            });
        }

        expr
    }

    fn addition(&mut self) -> Expr {
        let mut expr: Expr = self.multiplication();

        while self.match_type(&[&TokenType::MINUS, &TokenType::PLUS]) {
            let operator: Token = self.previous();
            let right: Expr = self.multiplication();
            expr = Expr::BinaryExpr(Binary {
                left: Rc::new(RefCell::new(expr)),
                operator,
                right: Rc::new(RefCell::new(right)),
            });
        }

        expr
    }

    fn comparison(&mut self) -> Expr {
        let mut expr: Expr = self.addition();

        while self.match_type(&[&TokenType::GREATER, &TokenType::GREATER_EQUAL, &TokenType::LESS, &TokenType::LESS_EQUAL]) {
            let operator: Token = self.previous();
            let right: Expr = self.addition();
            expr = Expr::BinaryExpr(Binary {
                left: Rc::new(RefCell::new(expr)),
                operator,
                right: Rc::new(RefCell::new(right)),
            });
        }

        expr
    }

    fn is_at_end(&self) -> bool {
        self.peek().token_type == TokenType::EOF
    }

    fn peek(&self) -> Token {
        self.tokens.get(self.current).unwrap().clone()
    }

    fn check(&self, t: &TokenType) -> bool {
        if self.is_at_end() { return false; }
        self.peek().token_type == *t
    }

    fn advance(&mut self) -> Token {
        if !self.is_at_end() { self.current += 1; }
        self.previous()
    }

    fn match_type(&mut self, types: &[&TokenType]) -> bool {
        for t in types {
            if self.check(t) {
                self.advance();
                return true;
            }
        }

        false
    }

    fn previous(&self) -> Token {
        self.tokens.get(self.current - 1).unwrap().clone()
    }

    fn equality(&mut self) -> Expr {
        let mut expr: Expr = self.comparison();

        while self.match_type(&[&TokenType::BANG_EQUAL, &TokenType::EQUAL_EQUAL]) {
            let operator: Token = self.previous();
            let right: Expr = self.comparison();

            expr = Expr::BinaryExpr(expression::Binary {
                left: Rc::new(RefCell::new(expr)),
                operator: operator.clone(),
                right: Rc::new(RefCell::new(right)),
            });
        }

        expr
    }

    fn expression(&mut self) -> Expr {
        self.equality()
    }
}
