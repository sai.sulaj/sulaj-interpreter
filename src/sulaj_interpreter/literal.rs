#[derive(Clone)]
pub enum Literal {
    Str(String),
    Num(f64),
    Bool(bool),
    None,
}
