use std::{
    collections::HashMap,
};

use crate::sulaj_interpreter::{
    token_type::TokenType,
    token::Token,
    literal::Literal,
    reserved,
};

pub struct Scanner {
    source: Box<Vec<char>>,
    tokens: Vec<Token>,
    start: usize,
    current: usize,
    line: usize,
    had_error: bool,
}

impl Scanner {
    pub fn new(source: &str) -> Self {
        Scanner {
            source: Box::new(source.chars().collect()),
            tokens: Vec::new(),
            start: 0,
            current: 0,
            line: 1,
            had_error: false,
        }
    }

    pub fn scan_tokens(&mut self, output: &mut Vec<Token>) -> bool {
        let reserved_keywords = reserved::get_reserved();

        while !self.is_at_end() {
            self.start = self.current;
            self.scan_token(&reserved_keywords);
        }
        self.tokens.iter().for_each(|token| {
            output.push(token.clone());
        });

        output.push(Token::new(TokenType::EOF, "".to_owned(), None, self.line));
        self.had_error
    }

    fn error(&mut self, line: usize, message: &str) {
        println!("[Line {}] Error: {}", line, message);
    }

    fn scan_token(&mut self, reserved_keywords: &HashMap<String, TokenType>) {
        let c: char = self.advance();
        match c {
            '(' => self.add_token(TokenType::LEFT_PAREN, None),
            ')' => self.add_token(TokenType::RIGHT_PAREN, None),
            '{' => self.add_token(TokenType::LEFT_BRACE, None),
            '}' => self.add_token(TokenType::RIGHT_BRACE, None),
            ',' => self.add_token(TokenType::COMMA, None),
            '.' => self.add_token(TokenType::DOT, None),
            '-' => self.add_token(TokenType::MINUS, None),
            '+' => self.add_token(TokenType::PLUS, None),
            ';' => self.add_token(TokenType::SEMICOLON, None),
            '*' => self.add_token(TokenType::STAR, None),
            '!' => {
                let next: TokenType;
                if self.match_peek('=') {
                    next = TokenType::BANG_EQUAL;
                } else {
                    next = TokenType::BANG;
                }
                self.add_token(next, None)
            },
            '=' => {
                let next: TokenType;
                if self.match_peek('=') {
                    next = TokenType::EQUAL_EQUAL;
                } else {
                    next = TokenType::EQUAL;
                }
                self.add_token(next, None)
            },
            '<' => {
                let next: TokenType;
                if self.match_peek('=') {
                    next = TokenType::LESS_EQUAL;
                } else {
                    next = TokenType::LESS;
                }
                self.add_token(next, None)
            },
            '>' => {
                let next: TokenType;
                if self.match_peek('=') {
                    next = TokenType::GREATER_EQUAL;
                } else {
                    next = TokenType::GREATER;
                }
                self.add_token(next, None)
            },
            'o' => {
                if self.match_peek('r') {
                    self.add_token(TokenType::OR, None)
                }
            },
            '/' => {
                if self.match_peek('/') {
                    while self.peek() != '\n' && !self.is_at_end() { self.advance(); }
                } else {
                    self.add_token(TokenType::SLASH, None);
                }
            },
            ' ' | '\r' | '\t' => (),
            '\n' => self.line += 1,
            '"' => self.string(),
            _ => {
                if self.is_digit(c) {
                    self.number();
                } else if self.is_alpha(c) {
                    self.identifier(reserved_keywords);
                } else {
                    self.error(self.line, "Unexpected character");
                }
            },
        }
    }

    fn identifier(&mut self, reserved_keywords: &HashMap<String, TokenType>) {
        while self.is_alpha_numeric(self.peek()) { self.advance(); }

        let text: String = self.source[self.start..self.current].into_iter().collect();
        match reserved_keywords.get(&text) {
            Some(token_type) => {
                self.add_token((*token_type).clone(), None);
            },
            None => {
                self.add_token(TokenType::IDENTIFIER, None);
            },
        };
    }

    fn is_alpha_numeric(&self, c: char) -> bool {
        self.is_alpha(c) || self.is_digit(c)
    }

    fn is_alpha(&self, c: char) -> bool {
        (c >= 'a' && c <= 'z')
        || (c >= 'A' && c <= 'Z')
        || c == '_'
    }

    fn is_digit(&self, c: char) -> bool {
        c >= '0' && c <= '9'
    }

    fn number(&mut self) {
        while self.is_digit(self.peek()) { self.advance(); }

        // Look for fractional part.
        if self.peek() == '.' && self.is_digit(self.peek_next()) {
            // Consume ".".
            self.advance();

            while self.is_digit(self.peek()) { self.advance(); }
        }

        let text: String = self.source[(self.start)..(self.current)].into_iter().collect();
        let num: f64 = text.parse().unwrap();
        self.add_token(TokenType::NUMBER, Some(Literal::Num(num)));
    }

    fn peek_next(&self) -> char {
        if self.current + 1 >= self.source.len() { return '\0'; }
        self.source.get(self.current).unwrap().to_owned()
    }

    fn string(&mut self) {
        while self.peek() != '"' && !self.is_at_end() {
            if self.peek() == '\n' { self.line += 1; }
            self.advance();
        }

        // Unterminated string.
        if self.is_at_end() {
            self.error(self.line, "Unterminated String.");
            return;
        }

        // The closing ".
        self.advance();

        // Trim the surrounding quotes.
        let text: String = self.source[self.start + 1..self.current - 1].into_iter().collect();
        self.add_token(TokenType::STRING, Some(Literal::Str(text)));
    }

    fn peek(&self) -> char {
        if self.is_at_end() { return '\0'; }
        self.source.get(self.current).unwrap().to_owned()
    }

    fn match_peek(&mut self, expected: char) -> bool {
        if self.is_at_end() { return false; }
        if self.source.get(self.current).unwrap() == &expected { return false; }

        self.current += 1;
        true
    }

    fn add_token(&mut self, token_type: TokenType, literal: Option<Literal>) {
        let text: String = self.source[self.start..self.current].into_iter().collect();
        self.tokens.push(Token::new(token_type, text, literal, self.line));
    }

    fn advance(&mut self) -> char {
        self.current += 1;
        self.source.get(self.current - 1).unwrap().to_owned()
    }

    fn is_at_end(&self) -> bool {
        self.current >= self.source.len()
    }
}
