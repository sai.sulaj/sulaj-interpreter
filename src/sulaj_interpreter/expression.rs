use std::{
    rc::Rc,
    cell::RefCell,
};

use crate::sulaj_interpreter::{
    token::Token,
    literal,
};

type NestedExpr = Rc<RefCell<Expr>>;

pub struct Binary {
    pub left: NestedExpr,
    pub operator: Token,
    pub right: NestedExpr,
}
pub struct Grouping {
    pub expression: NestedExpr,
}
pub struct Literal {
    pub value: literal::Literal,
}
pub struct Unary {
    pub operator: Token,
    pub right: NestedExpr,
}

pub enum Expr {
    BinaryExpr(Binary),
    GroupingExpr(Grouping),
    LiteralExpr(Literal),
    UnaryExpr(Unary),
}

pub trait Visitor<T> {
    fn visit_expr(&mut self, e: &Expr) -> T;
}
